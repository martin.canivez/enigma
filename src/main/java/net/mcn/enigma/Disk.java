package net.mcn.enigma;

import org.apache.commons.collections4.BidiMap;

import java.util.Map;

public class Disk extends EnigmaPiece
{
    private BidiMap<Integer, Integer> charMap;
    private int notchChar;
    private int currChar;
    private EnigmaPiece nextDisk;

    public Disk(Map<Character, Character> charMap, char notchChar, char currChar)
    {
        this.charMap = EnigmaUtil.transformMap(charMap);
        this.currChar = EnigmaUtil.alphabetPlace(currChar);
        this.notchChar = EnigmaUtil.alphabetPlace(notchChar);
    }

    public Disk(Map<Character, Character> charMap, char notchChar, char currChar, EnigmaPiece nextDisk) {
        this(charMap, notchChar, currChar);
        this.nextDisk = nextDisk;
    }

    public void setNextDisk(EnigmaPiece nextDisk) {
        this.nextDisk = nextDisk;
    }

    @Override
    public void forward()
    {
        if (currChar == notchChar && nextDisk != null)
            nextDisk.forward();

        currChar = (currChar+1)%26;
    }

    @Override
    public void backward()
    {
        currChar = (currChar+25)%26;

        if (currChar == notchChar && nextDisk != null)
            nextDisk.backward();
    }

    @Override
    int transform(int letter)
    {
        int outLetter = (currChar + letter) % 26;
        outLetter = (charMap.get(outLetter)+26-currChar) % 26;

        if (nextDisk != null)
        {
            outLetter = nextDisk.transform(outLetter);
            outLetter = charMap.getKey((outLetter+ currChar) % 26);
            outLetter = (outLetter+26-currChar) % 26;
        }

        return outLetter;
    }

    public char code(char letter)
    {
        forward();
        return EnigmaUtil.alphabetLetter(transform(EnigmaUtil.alphabetPlace(letter)));
    }
}
