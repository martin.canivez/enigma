package net.mcn.enigma;

import javafx.util.Pair;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class EnigmaUtil
{
    static BidiMap<Integer, Integer> transformMap(Map<Character, Character> charMap)
    {
        return new DualHashBidiMap<>(charMap.entrySet()
                .parallelStream()
                .map(e -> new Pair<>(e.getKey()-'A', e.getValue()-'A'))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue)));
    }

    static int alphabetPlace(char c)
    {
        return c-'A';
    }

    static char alphabetLetter(int p)
    {
        return (char) (p+'A');
    }

    static Map<Character, Character> stringToRotor(String s)
    {
        String c[] = s.split("->");

        Map<Character, Character> m = new HashMap<>();

        for (int i = 0; i < Math.min(c[0].length(), c[1].length()); i++)
            m.put(c[0].charAt(i), c[1].charAt(i));

        return m;
    }
}
