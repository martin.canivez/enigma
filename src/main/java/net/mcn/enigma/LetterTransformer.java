package net.mcn.enigma;

public interface LetterTransformer
{
    char code(char letter);
}
