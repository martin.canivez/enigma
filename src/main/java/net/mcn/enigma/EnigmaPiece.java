package net.mcn.enigma;

abstract public class EnigmaPiece implements LetterTransformer, Turntable
{
    abstract int transform(int letter);

    public String code(String input)
    {
        input = input.toUpperCase().replaceAll("[^A-Z]", "");

        StringBuilder sb = new StringBuilder(input.length());

        for(int i = 0; i < input.length(); i++)
            sb.append(code(input.charAt(i))).append(i % 4 == 3 ? " " : "");

        return sb.toString();
    }
}
