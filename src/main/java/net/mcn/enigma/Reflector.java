package net.mcn.enigma;

import java.util.Map;

public class Reflector extends EnigmaPiece
{
    private Map<Integer, Integer> rotorMap;

    public Reflector(Map<Character, Character> rotorMap)
    {
        this.rotorMap = EnigmaUtil.transformMap(rotorMap);
    }

    public static Reflector standard()
    {
        return new Reflector(EnigmaUtil.stringToRotor("ABCDEFGHIJKLMNOPQRSTUVWXYZ->YRUHQSLDPXNGOKMIEBFZCWVJAT"));
    }

    @Override
    int transform(int letter)
    {
        return rotorMap.get(letter);
    }

    @Override
    public char code(char letter)
    {
        return EnigmaUtil.alphabetLetter(transform(EnigmaUtil.alphabetPlace(letter)));
    }

    @Override
    public void forward() {

    }

    @Override
    public void backward() {

    }
}
