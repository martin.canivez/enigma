package net.mcn.enigma;

public interface Turntable
{
    void forward();

    void backward();
}
