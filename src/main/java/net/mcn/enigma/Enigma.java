package net.mcn.enigma;

public class Enigma
{
    public static void main(String[] args)
    {
        Disk d1 = new Disk(EnigmaUtil.stringToRotor("ABCDEFGHIJKLMNOPQRSTUVWXYZ->EKMFLGDQVZNTOWYHXUSPAIBRCJ"), 'Q', 'Z');
        Disk d2 = new Disk(EnigmaUtil.stringToRotor("ABCDEFGHIJKLMNOPQRSTUVWXYZ->AJDKSIRUXBLHWTMCQGZNPYFVOE"), 'E', 'H');
        Disk d3 = new Disk(EnigmaUtil.stringToRotor("ABCDEFGHIJKLMNOPQRSTUVWXYZ->BDFHJLCPRTXVZNYEIWGAKMUSQO"), 'V', 'A');
        Reflector r = Reflector.standard();

        d1.setNextDisk(r);
        d2.setNextDisk(d1);
        d3.setNextDisk(d2);

        System.out.println(d3.code("BPBWSKTMOKTMOIDTQQJTYXDQFZJRWFRSEZCU"));
    }
}
